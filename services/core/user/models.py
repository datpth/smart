from rest_framework_jwt.settings import api_settings

from django.db import models
from django.utils.translation import ugettext_lazy as _

from services.core.models import BaseModel

from smart.settings import SECRET_KEY

from datetime import datetime, timedelta


class User(BaseModel):
    username = models.CharField(
        max_length=50, db_column='username', blank=False, null=False, verbose_name=_('Username'))
    password = models.CharField(max_length=200, db_column='password',
                                blank=False, null=False, verbose_name=_('Password'))

    email = models.CharField(max_length=50, db_column='email',
                             blank=False, null=False, verbose_name=_('Password'))

    mobile1 = models.CharField(
        max_length=50, db_column='mobile1', blank=True, null=True, verbose_name=_('Mobile'))

    mobile2 = models.CharField(
        max_length=50, db_column='mobile2', blank=True, null=True, verbose_name=_('Mobile'))

    token = models.CharField(max_length=50, db_column='token',
                             blank=True, null=True, verbose_name=_('Token'))
    token_date = models.DateField(
        db_column='token_date', blank=True, null=True, verbose_name=_('Token Date'))

    avatar = models.ImageField(upload_to='profile/user/avatar/', db_column='avatar_url', blank=True,
                               null=True,
                               verbose_name=_('Avatar'))

    last_login = models.DateTimeField(
        db_column='last_login', blank=True, null=True, verbose_name=_('Last Login'))

    is_active = models.BooleanField(
        db_column='is_active', default=True, verbose_name=_('Is Active'))

    class Meta:
        db_table = 'minerva_smart_user'
        verbose_name_plural = _('User')

    @property
    def token(self):
        """
        Allows us to get a user's token by calling `user.token` instead of
        `user.generate_jwt_token().

        The `@property` decorator above makes this possible. `token` is called
        a "dynamic property".
        """
        return self._generate_jwt_token()

    def _generate_jwt_token(self):
        """
        Generates a JSON Web Token that stores this user's ID and has an expiry
        date set to 60 days into the future.
        """

        payload = api_settings.JWT_PAYLOAD_HANDLER(self)
        token = api_settings.JWT_ENCODE_HANDLER(payload)

        return token
