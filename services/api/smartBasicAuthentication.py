import base64
import binascii
import json

from django.utils.translation import gettext_lazy as _
from django.contrib.auth.hashers import check_password

from rest_framework.permissions import IsAuthenticated
from rest_framework.authentication import BasicAuthentication
from rest_framework import status
from rest_framework.response import Response

from services.core.user.models import User

from library.functions import now


def authenticate(**credentials):
    try:
        user = User.objects.get(username=credentials['username'])
        password = credentials['password']
        if check_password(password, user.password):
            return user, None
        else:
            if user:
                return None, status.HTTP_403_FORBIDDEN
            return None, status.HTTP_404_NOT_FOUND
    except User.DoesNotExist:
        return None, status.HTTP_404_NOT_FOUND


class SmartBasicAuthentication(BasicAuthentication):
    def authenticate_credentials(self, username, password, request=None):
        credentials = {
            'username': username,
            'password': password
        }

        user, _ = authenticate(**credentials)
        if user and user.is_active:
            user.last_login = now()
            user.save()
        setattr(request, 'user', user)

        return user, None  # authentication successful


