class APICheckApiInvalidException(Exception):

    def __init__(self, message={}):
        self.message = message

    def __str__(self):
        print(self.message)
