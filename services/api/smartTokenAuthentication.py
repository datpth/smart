from rest_framework_jwt.settings import api_settings

import json

from rest_framework import status, exceptions
from rest_framework.response import Response
from rest_framework.authentication import get_authorization_header, TokenAuthentication
from rest_framework import status

from services.core.user.models import User

from smart.settings import SECRET_KEY


class SmartTokenAuthentication(TokenAuthentication):
    model = None
    keyword = 'Bearer'

    def get_model(self):
        return User

    def authenticate(self, request):
        auth = get_authorization_header(request).split()
        if not auth or auth[0].lower() != b'bearer':
            return None
        if len(auth) == 1:
            msg = 'Invalid token header. No credentials provided.'
            raise exceptions.AuthenticationFailed(msg)
        elif len(auth) > 2:
            msg = 'Invalid token header'
            raise exceptions.AuthenticationFailed(msg)
        try:
            token = auth[1]
        except UnicodeError:
            msg = _(
                'Invalid token header. Token string should not contain invalid characters.')
            raise exceptions.AuthenticationFailed(msg)
        return self.authenticate_credentials(token, request)

    def authenticate_credentials(self, token, request=None):
        model = self.get_model()

        try:
            payload = api_settings.JWT_DECODE_HANDLER(token)
        except:
            payload = None

        userid = payload.get('user_id') if payload else None
        status_code = None
        try:
            user = model.objects.get(
                id=userid,
                is_active=True
            )
        except User.DoesNotExist:
            user = None
            status_code = status.HTTP_404_NOT_FOUND

        setattr(request, 'status_code', status_code)
        setattr(request, 'user', user)

        return (user, token)

    def authenticate_header(self, request):
        return self.keyword
