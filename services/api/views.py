from rest_framework.views import APIView
from rest_framework.response import Response

from services.api.smartTokenAuthentication import SmartTokenAuthentication
from services.api.exceptions import APICheckApiInvalidException


class APIView(APIView):
    authentication_classes = (SmartTokenAuthentication,)

    def initial(self, request, *args, **kwargs):
        super().initial(request, *args, **kwargs)
        self.check_token()

    def handle_exception(self, exc):
        if isinstance(exc, APICheckApiInvalidException):
            return Response(exc.message)
        return super().handle_exception(exc)

    def check_token(self):
        request = self.request
        check = None
        try:
            user = request.user
            status_code = request.status_code
            if user is None:
                check = dict(
                    success=False,
                    status=status_code
                )
        except:
            check = dict(
                success=False,
                status=status_code
            )

        if check:
            raise APICheckApiInvalidException(message=check)
