from django.conf.urls import include
from django.urls import path

urlpatterns = [
    path('login/', include('services.api.v1.login.urls')),
    path('sign_up/', include('services.api.v1.sign_up.urls')),
]
