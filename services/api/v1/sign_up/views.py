from services.api.views import APIView

from rest_framework.response import Response
from services.core.user.models import User

from django.contrib.auth.hashers import make_password

from services.api.smartTokenAuthentication import SmartTokenAuthentication


class SignUpView(APIView):

    def post(self, request, *args, **kwargs):

        return Response(dict(
            success='OK'
        ))
