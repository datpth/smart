from django.conf.urls import include
from django.urls import path
from services.api.v1.sign_up.views import SignUpView

urlpatterns = [
    path('', SignUpView.as_view()),
]
