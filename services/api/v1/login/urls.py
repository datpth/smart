from django.conf.urls import include
from django.urls import path
from services.api.v1.login.views import LoginView

urlpatterns = [
    path('', LoginView.as_view()),
]
