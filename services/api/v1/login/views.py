import json

from django.http import HttpResponse

from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated
from rest_framework import status

from services.core.user.models import User

from smart.settings import SECRET_KEY


class LoginView(APIView):
    def post(self, request, *args, **kwargs):
        user = request.user
        if user is None:
            return Response(
                dict(success=False,
                     detail='Username/Password does not exist!'),
                status=status.HTTP_400_BAD_REQUEST,
                content_type="application/json"
            )
        try:
            user = User.objects.get(
                pk=user.id)

            jwt_token = dict(token=user.token)

            return Response(
                jwt_token,
                status=status.HTTP_200_OK,
                content_type="application/json")
        except User.DoesNotExist:
            return Response(
                dict(success=False,
                     detail='Invalid credentials!'),
                status=status.HTTP_400_BAD_REQUEST,
                content_type="application/json"
            )
